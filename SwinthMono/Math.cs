﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SwinthMono
{
    static class Calculations
    {
        public const int letterWidth = 34; //determined by visually seeing that 40 chars took 1440 pixels
        public const int letterHeight = 32;
        public const int maxColumns = 40; //c64 default
        public const int maxRows = 25; //c64 default
        /// <summary>
        /// computes the width scaling necessary to squeeze 40 columns into the current box size.
        /// note that it also cares what the height is and will introduce margins since you can't 
        /// skew the scaling on a font more in x vs y and vice versa.  This is fine since the
        /// original program I am cloning had borders anyway.  For brevity, we assume square pixels
        /// </summary>
        /// <returns></returns>
        public static float GetWidthScale() { return Properties.Settings.Default.Resolution.Width / getMaxStringLength(); }
        public static float GetHeightScale() { return Properties.Settings.Default.Resolution.Height / getMaxStringHeight(); }
        private static float getMaxStringLength() { return letterWidth * maxColumns; }
        private static float getMaxStringHeight() { return letterHeight * maxRows; }
        public static float getScaledMaxStringLength() { return getMaxStringLength() * getTextViewportScale(); }
        public static float getScaledStringLength(string text)
        {
            return getScaledMaxStringLength() / 40 * text.Length;
        }
        public static float getUnderlineOffset()
        {
            return (letterHeight + 4) * getTextViewportScale();
        }
        public static float getTextViewportScale() 
        {
            float ws = GetWidthScale();
            float hs = GetHeightScale();
            return System.Math.Min(ws, hs);
        }
        public static int getMarginWidth() { return (int)(Properties.Settings.Default.Resolution.Width - getScaledMaxStringLength())/2; }
        
        public static Vector2 ShrinkHoriz(Vector2 vector2)
        {
            return new Vector2(vector2.X / 2, vector2.Y);
        }
        public static Vector2 ShrinkVert(Vector2 vector2)
        {
            return new Vector2(vector2.X, vector2.Y / 2);
        }
        public static Vector2 FlipHoriz(Vector2 vector2)
        {
            return new Vector2(Properties.Settings.Default.Resolution.Width - vector2.X, vector2.Y);
        }
        public static Vector2 FlipVert(Vector2 vector2)
        {
            return new Vector2(vector2.X, Properties.Settings.Default.Resolution.Height - vector2.Y);
        }
    }
}
