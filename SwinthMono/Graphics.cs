﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SwinthMono
{
    class Graphics
    {
        public enum ResolutionChange
        {
            Larger, Smaller, Initial, Exact
        };
        private static DisplayMode[] supportedResolutions; //default adapter's supported resolutions.  
      
        private static GraphicsDeviceManager graphics;
        private static System.Windows.Forms.Screen[] allScreens; //array of all attached displays with relationship
        static public void Init(SwinthMono.Main m)
        {
            graphics = new GraphicsDeviceManager(m);
            graphics.PreparingDeviceSettings += (s, e) =>
            {
                e.GraphicsDeviceInformation.GraphicsProfile = GraphicsProfile.HiDef;
            };
            graphics.IsFullScreen = Properties.Settings.Default.FullScreen;

            supportedResolutions = GraphicsAdapter.DefaultAdapter.SupportedDisplayModes
                .Select(x => x)
                .OrderBy(x => x.Width * x.Height)
                .ToArray();
            SetResolution(ResolutionChange.Initial);
            graphics.ApplyChanges();

            allScreens = System.Windows.Forms.Screen.AllScreens;
        }

        public static int GetMatchingResolutionIndex()
        {
            for (int cl = 0; cl < supportedResolutions.Length; cl++) 
            {
                if (supportedResolutions[cl].Width == Properties.Settings.Default.Resolution.Width
                    && supportedResolutions[cl].Height == Properties.Settings.Default.Resolution.Height)
                {
                    return cl;
                }            
            }   
            return -1;
        }                

        /// <summary>
        ///     Sets screen resolution.  
        /// </summary>
        /// <param name="type"></param>
        /// <returns>the index of the resolution you switched to, or current if it didn't change.</returns>
        public static int SetResolution(ResolutionChange type, int index = 0)
        {
            int curr = GetMatchingResolutionIndex();
            if (type == ResolutionChange.Initial)
            {
                if (curr == -1) //maybe you swapped monitors or something so use smallest supported
                    curr = 0;
                return SetResolution(ResolutionChange.Exact, curr);
            }
            else if (type == ResolutionChange.Larger)
            {
                if (curr >= supportedResolutions.GetUpperBound(0)) //no change
                    return curr;
                //recursively call self to set resolution
                return SetResolution(ResolutionChange.Exact, curr + 1);
            }
            else if (type == ResolutionChange.Smaller)
            {
                if (curr == 0) //no change
                    return curr;
                //recursively call self to set resolution
                return SetResolution(ResolutionChange.Exact, curr - 1);
            }
            else //if (type == ResolutionChange.Exact)
            {
                graphics.PreferredBackBufferWidth = supportedResolutions[index].Width;
                graphics.PreferredBackBufferHeight = supportedResolutions[index].Height;
                graphics.ApplyChanges();
                Size s = new Size() { Height = graphics.PreferredBackBufferHeight, Width = graphics.PreferredBackBufferWidth };
                Properties.Settings.Default.Resolution = s;
                return curr;
            }
                
        }
        static public void EnableFakeFullScreen(System.Windows.Forms.Form form)
        {
            graphics.IsFullScreen = false;
            graphics.ApplyChanges();
            //get dimensions of box that will cover all displays and set window to it.
            int xPos = System.Windows.Forms.Screen.AllScreens.OrderBy(x => x.Bounds.X).Select(x => x.Bounds.X).First();
            int yPos = System.Windows.Forms.Screen.AllScreens.OrderBy(y => y.Bounds.Y).Select(y => y.Bounds.Y).First();
            form.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            form.Location = new System.Drawing.Point(xPos, yPos);
            int xWidth = System.Windows.Forms.Screen.AllScreens.OrderByDescending(x => x.Bounds.X).Select(x => x.Bounds.X + x.Bounds.Width).First() - xPos;
            int yHeight = System.Windows.Forms.Screen.AllScreens.OrderByDescending(y => y.Bounds.Y).Select(y => y.Bounds.Y + y.Bounds.Height).First() - yPos;

              graphics.PreferredBackBufferWidth = xWidth;
              graphics.PreferredBackBufferHeight = yHeight;
              graphics.ApplyChanges();
              Properties.Settings.Default.WindowLocation = form.Location;
            Properties.Settings.Default.Resolution = form.Size;
            //Properties.Settings.Default.FakeFullScreen = true;
        }
        static public bool ToggleFullScreen()
        {
            if (graphics.IsFullScreen)
            {
                graphics.IsFullScreen = false;
            }
            else
            {
                graphics.IsFullScreen = true;
            }
            graphics.ApplyChanges();
            Thread.Sleep(1);
            Properties.Settings.Default.FullScreen = graphics.IsFullScreen;
            return graphics.IsFullScreen;
        }
    }
}
