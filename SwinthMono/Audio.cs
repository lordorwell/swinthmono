﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;


namespace SwinthMono
{
    class Audio
    {
        public static SoundEffect[] songs;
        public static SoundEffectInstance bgMusic;
        public static void LoadContent(ContentManager content)
        {
            /*I don't use "Song" instead of SoundEffect because it uses WMP libraries
             under the hood, which means it will not run on some systems including mine*/
            songs = new SoundEffect[3] 
                        { 
                            content.Load<SoundEffect>("Audio/1"), 
                            content.Load<SoundEffect>("Audio/2"), 
                            content.Load<SoundEffect>("Audio/3")
                        };
          
        }
        public static void PlaySong(int index) 
        {   
            bgMusic?.Dispose();
            bgMusic = songs[index].CreateInstance();
            bgMusic.Play();
        }
        public static void PauseSong()
        {
            bgMusic.Pause();
        }
        public static void PlayNextSong() //take advantage of my ContinuousPlay loop
        { 
            bgMusic.Stop();
        }
            /// <summary>
        /// this should be polled from the main game loop.
        /// It will handle all playing logic.
        /// </summary>
        public static void ContinuousPLay()
        {
            if (bgMusic == null)
                PlaySong(Properties.Settings.Default.SongIndex);
            else if (bgMusic.State == SoundState.Stopped)
            {
                int tmp = Properties.Settings.Default.SongIndex;
                tmp++;
                if (tmp > songs.GetUpperBound(0))
                {
                    tmp = 0;
                }
                Properties.Settings.Default.SongIndex = tmp;
                PlaySong(tmp);
            }
            else if (bgMusic.State == SoundState.Paused)
            {
                bgMusic.Resume();
            }
        }
    }
}
