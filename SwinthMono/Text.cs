﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace SwinthMono
{
    public class Text
    {  
        static SpriteFont fontZig;
        public static void LoadContent(ContentManager content)
        {
            //zig is a retro styled spritefont
            fontZig = content.Load<SpriteFont>("Fonts/Zig");
        }
        static public void DrawText(string text, int xPos, int yPos, SpriteBatch spriteBatch, bool underline = false)
        {
            xPos = xPos + Calculations.getMarginWidth();
            yPos = (int)(yPos * Calculations.getTextViewportScale()); //prevent weird gaps in lines when changing resolutions
            xPos = (int)(xPos * Calculations.getTextViewportScale()); //prevent weird gaps in lines when changing resolutions
            spriteBatch.DrawString(fontZig, text, new Vector2(xPos, yPos), Color.CornflowerBlue, 0, new Vector2(0, 0), Calculations.getTextViewportScale(), SpriteEffects.None, 1);
            if (underline)
            {
                Drawing.DrawLine(spriteBatch, new Vector2(xPos, yPos + Calculations.getUnderlineOffset()), new Vector2(xPos + Calculations.getScaledStringLength(text), yPos + Calculations.getUnderlineOffset()), new Pen(Color.CornflowerBlue, 3));
            }
        }
        static public void DrawTextByColumnRow(string text, int column, int row, SpriteBatch spriteBatch, bool underline = false)
        {
            int xPos = Calculations.letterWidth * (column-1);
            int yPos = Calculations.letterHeight * (row-1);
            DrawText(text, xPos, yPos, spriteBatch, underline);
        }
        static public void DrawTextByRowCentered(string text, int row, SpriteBatch spriteBatch, bool underline = false) 
        {
            int xPos = (Calculations.maxColumns - text.Length) / 2 + 1 ; //the text grid is one-based, not zero
            int yPos = row ;
            DrawTextByColumnRow(text, xPos, yPos, spriteBatch, underline);
        }
        static public void Menu(SpriteBatch spriteBatch)
        {
            DrawTextByRowCentered("Swinth", 2, spriteBatch, true);
            DrawTextByRowCentered("Hit SpaceBar to Start", 5, spriteBatch);
            DrawTextByColumnRow("I     changes max increment 32>16>8>4", 1, 7, spriteBatch);
            DrawTextByColumnRow("e     toggles min increment 1<-->2", 1, 8, spriteBatch);
            DrawTextByColumnRow("h     toggles horizontal symmetry", 1, 9, spriteBatch);
            DrawTextByColumnRow("v     toggles vertical symmetry", 1,10, spriteBatch);
            DrawTextByColumnRow("f1    changes color #1", 1,11, spriteBatch);
            DrawTextByColumnRow("f2    changes color #2", 1,12, spriteBatch);
            DrawTextByColumnRow("f3    changes color #3", 1,13, spriteBatch);
            DrawTextByColumnRow("f4    toggles random colors", 1,14, spriteBatch);
            DrawTextByColumnRow("0-9   changes length of tail", 1,15, spriteBatch);
            DrawTextByColumnRow("esc   terminates program", 1, 16, spriteBatch);
            DrawTextByColumnRow("space plays the next song", 1, 17, spriteBatch);
            DrawTextByRowCentered("music from synth sample", 20, spriteBatch);


        }
    }
  
}
