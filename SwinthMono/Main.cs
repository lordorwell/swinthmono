﻿/*
 SwinthMono is a reasonable
 */

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Linq;
using System.Runtime.InteropServices;

namespace SwinthMono
{
    public enum GameMode 
    {
        Menu1,
        Menu2,
        Play,
        Pause
    }

    /// <summary>
    /// This is the main class for your game.
    /// </summary>
    public class Main : Game
    {
        System.Windows.Forms.Form form;
        SpriteBatch spriteBatch;
        GamePadState gamePadStored = new GamePadState();
        KeyboardState keyboardStored = new KeyboardState();
        private GameMode gameMode = GameMode.Menu1;
        public GameMode getGameMode() { return gameMode; }
        public Main()
        {
            Graphics.Init(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            
            base.Initialize();
            //get form object.  It's not normally given to us in a mono project.
            form = (System.Windows.Forms.Form)System.Windows.Forms.Control.FromHandle(this.Window.Handle);
            form.LocationChanged += (sender, e) => 
            {
                Properties.Settings.Default.WindowLocation = form.Location;
            };
            form.Location = Properties.Settings.Default.WindowLocation;

        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            Text.LoadContent(Content);
            Audio.LoadContent(Content);
            Drawing.initialize(GraphicsDevice);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here

            Properties.Settings.Default.Save();
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            //comparing to stored to prevent repeated triggers.  One call per press please.
            GamePadState gamePad = GamePad.GetState(PlayerIndex.One);
            KeyboardState keyboard = Keyboard.GetState();
            if (gamePad != gamePadStored) 
            {
                gamePadStored = gamePad;
                if (gamePad.Buttons.Back == ButtonState.Pressed)
                {
                    Exit();
                }
            }
            if (keyboard != keyboardStored)
            {
                keyboardStored = keyboard;
                if (keyboard.IsKeyDown(Keys.Escape))
                {
                    if (gameMode == GameMode.Menu1 || gameMode == GameMode.Menu2)
                        Exit();
                    else
                    {
                        gameMode = GameMode.Menu1;
                        Audio.PauseSong();
                    }
                }
                else if (keyboard.IsKeyDown(Keys.F1))
                {
                    Drawing.IncrementColor(0);
                }
                else if (keyboard.IsKeyDown(Keys.F2))
                {
                    Drawing.IncrementColor(1);
                }
                else if (keyboard.IsKeyDown(Keys.F3))
                {
                    Drawing.IncrementColor(2);
                }
                //detect all number keys at once in either area
                for (int i = 0; i < 10; i++)
                { 
                    Keys topRowKey;
                    Keys keypadKey;
                    Enum.TryParse((48 + i).ToString(), out topRowKey);
                    Enum.TryParse((96 + i).ToString(), out keypadKey);
                    if (keyboard.IsKeyDown(topRowKey) || keyboard.IsKeyDown(keypadKey))
                    {
                        Drawing.SetTailLength(i);
                    }
                }
                if (keyboard.IsKeyDown(Keys.P))
                {
                    Drawing.TogglePaused();
                }
                else if (keyboard.IsKeyDown(Keys.F))
                {
                    if (keyboard.IsKeyDown(Keys.LeftControl) || keyboard.IsKeyDown(Keys.RightControl))
                    {
                        Graphics.EnableFakeFullScreen(form);
                    }
                    else
                    {
                        Graphics.ToggleFullScreen();
                    }
                }
                else if (keyboard.IsKeyDown(Keys.H))
                {
                    Drawing.ToggleHMirror();
                }
                else if (keyboard.IsKeyDown(Keys.V))
                {
                    Drawing.ToggleVMirror();
                }
                else if (keyboard.IsKeyDown(Keys.I))
                {
                    Drawing.CycleMaxIncrement();
                }
                else if (keyboard.IsKeyDown(Keys.E))
                {
                    Drawing.CycleMinIncrement();
                }
                else if (keyboard.IsKeyDown(Keys.C))
                {
                    Drawing.CycleDrawMode();
                }
                else if (keyboard.IsKeyDown(Keys.O))
                {
                    Drawing.ToggleOverlap();
                }
                else if (keyboard.IsKeyDown(Keys.Add))
                {
                    Graphics.SetResolution(Graphics.ResolutionChange.Larger);
                }
                else if (keyboard.IsKeyDown(Keys.Subtract))
                {
                    Graphics.SetResolution(Graphics.ResolutionChange.Smaller);
                }
                else if (keyboard.IsKeyDown(Keys.Space))
                {
                    if (gameMode == GameMode.Menu1 || gameMode == GameMode.Menu2)
                    {
                        Audio.PlaySong(0);
                        gameMode = GameMode.Play;
                    }
                    else if (gameMode == GameMode.Play)
                    {
                        Audio.PlayNextSong();
                    }
                }
            }
            if (gameMode == GameMode.Play) 
                Audio.ContinuousPLay();

            // TODO: Add your update logic here

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.TransparentBlack);

            // TODO: Add your drawing code here
            spriteBatch.Begin();
            if (gameMode == GameMode.Menu1 || gameMode == GameMode.Menu2)
                Text.Menu(spriteBatch);
            else
                Drawing.DrawTrail(spriteBatch);

            spriteBatch.End();
            
            base.Draw(gameTime);
        }
    }
}
