﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SwinthMono
{
    //magnitude is a custom object that is only used statically to store the direction the next line needs to move
    class Magnitude
    {

        public static Point start = new Point(9, 6);
        public static Point end = new Point(6, 9);

    }
    public class Pen
    {
        public Pen(Color color, int thickness)
        {
            this.color = color;
            this.thickness = thickness;
        }
        public Color color;
        public int thickness;
    }
    class Line //custom object to keep attributes that I need in the line trail
    {
        public Line()
        {
            start = new Vector2(150, 50);  //x & y of one end of the line.
            end = new Vector2(50, 150);    //x & y of the other end of the line.
        }
        public Vector2 start;
        public Vector2 end;
        public int colorIndex; //line color index.  valid values = 0-2.  The draw routine looks the pen up
        public Color rgbColor = Color.Black;
    }

    class Drawing
    {
        private static int[] colorIndexes = new int[3]; //the three indexes used for the on-screen colors.
        private static int nextColorCycle = 50; //after 50 lines are drawn, it goes to the next color, then changes this value to a random # from 20 to 100
        private static int colorStepsTaken = 0;
        private static int[] penThickness = new int[3]; //contains the color info for the three pens currently on screen

//        private static Font font;
//        private static Brush brush = new SolidBrush(Color.FromArgb(128, 128, 255));

        private static bool hMirror = true;
        private static bool vMirror = true;
        private static bool reverse = false; //reverses lines on split on one side

        private static bool gradientMode = true;
        public static int screenshotCounter = 1;

        private static int maxIncrement = 32;
        private static int minIncrement = 4;

        private static Random rnd = new Random();

        private static int width = 0; //for storing the dimensions of the draw area in
        private static int height = 0;

        private static Line[] trail;
        private static int trailLength = 72;
        private static int maxTrailLength = 720;
        private static int trailStartMarker = 0; //which index are you looking at 
        private static int trailEndMarker = 0;
        private static bool showEndCaps = false;
        private static bool classicErase = false;
        private static int divisor = 2; // if you set this to one, they overlap
        //a list of the only colors i'm interested in using (16 of them) in default mode
        private static List<Color> lstColors = new List<Color>(
            new Color[]{Color.Gray, Color.Brown, Color.DarkRed, Color.DarkOrange, Color.Green, Color.Blue, Color.Yellow, Color.DarkCyan,
            Color.LightGray, Color.SaddleBrown, Color.Red, Color.Orange, Color.LightGreen, Color.LightBlue, Color.LightYellow, Color.Cyan});

        //xna variables
        private static Texture2D t;
        /*
        * DrawText does what it says:  It draws text on the screen.  column defaults to -1 which means center it.
        * I have hard-coded 40 columns into the design so you'll see some math based on that.
        */
        public static void initialize(GraphicsDevice graphicsDevice)
        {
            t = new Texture2D(graphicsDevice, 1, 1);
            t.SetData<Color>(
            new Color[] { Color.White });// fill the texture with white


            penThickness[0] = 4; //initialize the pens.  
            penThickness[1] = 4; //to save memory, they are only created once each.
            penThickness[2] = 4;

            trail = new Line[maxTrailLength];//max trail length = maxTrailLength
            for (int i = 0; i < maxTrailLength; i++)
                trail[i] = new Line();

            SetPen(4, 0); //set starting values for the three colors we cycle through.
            SetPen(11, 1);
            SetPen(5, 2);
        }
        public static void SetPen(int colorNum, int index = 0)
        { //valid index = 0 to 2
          //valid colorNum = 0-15
          //   pen[index].Color = lstColors[colorNum];
            colorIndexes[index] = colorNum; //need it stored so i can cycle with it
        }
        public static void DrawLine(SpriteBatch spriteBatch, Vector2 start, Vector2 end, Pen pen)
        {
            Vector2 edge = end - start;
            // calculate angle to rotate line
            float angle = (float)Math.Atan2(edge.Y, edge.X);
            spriteBatch.Draw(t,
                new Rectangle(// rectangle defines shape of line and position of start of line
                    (int)start.X,
                    (int)start.Y,
                    (int)edge.Length(), //sb will strech the texture to fill this rectangle
                    pen.thickness), //width of line, change this to make thicker line
                null,
                pen.color, //colour of line
                angle,     //angle of line (calulated above)
                new Vector2(0, 0), // point in line about which to rotate
                SpriteEffects.None,
                0);

        }
        /// <summary>
        /// draws a rectangle.  will look into a better way later.  This seems to be the best because i only want the border.
        /// </summary>
        /// <param name="spriteBatch">Required.  The spritebatch of the main game loop.</param>
        /// <param name="start">a vector2 representing the upper left corner.</param>
        /// <param name="end">a vector2 representing the lower right corner.</param>
        /// <param name="pen">a custom class containing color and thickness.  It is here to "emulate" the pen in gdi.</param>
        public static void DrawRectangle(SpriteBatch spriteBatch, Vector2 start, Vector2 end, Pen pen)
        {
            DrawLine(spriteBatch, new Vector2(start.X, start.Y), new Vector2(end.X, start.Y), pen);
            DrawLine(spriteBatch, new Vector2(end.X, start.Y), new Vector2(end.X, end.Y), pen);
            DrawLine(spriteBatch, new Vector2(end.X, end.Y), new Vector2(start.X, end.Y), pen);
            DrawLine(spriteBatch, new Vector2(start.X, end.Y), new Vector2(start.X, start.Y), pen);
        }

        public static void ToggleHMirror() { hMirror = !hMirror; }

        //this toggles whether or not vertical split happens.  Called from 'v' keypress
        public static void ToggleVMirror() { vMirror = !vMirror; }

        public static void ToggleReverse() { reverse = !reverse; }


        public static void SetTailLength(int length)
        {
            Drawing.trailLength = length * 10; //gives a length from 10 to 90
        }
        //called by f1, f2, f3.  Cycles through the color list for one of the three colors
        public static void IncrementColor(int whichColor)
        {
            int index = colorIndexes[whichColor];
            index++;
            if (index > 15)
                index = 0;
            SetPen(index, whichColor);
        }
        /*
         * called from f-4 press.  This gets three random #s from 0-15, and sets it to each color.
         */
        public static void RandomizeColor()
        {
            SetPen(rnd.Next(0, 15), 0);
            SetPen(rnd.Next(0, 15), 1);
            SetPen(rnd.Next(0, 15), 2);

        }
        public static void ToggleOverlap()
        {
            divisor ^= 3;
        }
        public static void CyclePenThickness(int index)
        {
            if (Drawing.penThickness[index] < 6)//real-time. 
            {
                Drawing.penThickness[index]++;
            }
            else
            {
                Drawing.penThickness[index] = 1;
            }

        }
        //fixme:  drawing function doesn't currently support these
        public static void ToggleEndCaps()
        { showEndCaps = !showEndCaps; }
        public static void ToggleErasingTail()
        { classicErase = !classicErase; }
        public static void CycleMaxIncrement()
        {
            if (Drawing.maxIncrement < 64)//trickery to cycle powers of 2 from 4 to 64
            {
                Drawing.maxIncrement *= 2;
            }
            else
            {
                Drawing.maxIncrement = 4;
            }
        }
        public static void CycleMinIncrement()
        {
            Drawing.minIncrement ^= 6; //trickery to toggle between 2 and 4 by xoring 6
        }
        private static bool paused = false;
        public static void TogglePaused() { Drawing.paused = !Drawing.paused; }
        public static void ToggleGradientMode()
        {
            gradientMode = !gradientMode;
        }
        private static int drawMode = 0;
        public static void CycleDrawMode()
        {
            drawMode = ++drawMode % 3;
        }
        public static void DrawTrail(SpriteBatch spriteBatch)
        {
            Graphics g;
            //if (classicErase == true)
            //{
            //    try
            //    {
            //        g = Graphics.FromImage(Main.ActiveForm.BackgroundImage); //for drawing on the background image directly
            //        width = Main.ActiveForm.BackgroundImage.Width;
            //        height = Main.ActiveForm.BackgroundImage.Height;
            //    }
            //    catch (NullReferenceException)
            //    {
            //        return; //prevent a crash when you lose focus
            //    }
            //}
            //else
            {
                width = Properties.Settings.Default.Resolution.Width;
                height = Properties.Settings.Default.Resolution.Height;
            }
            if (paused == false) //if paused, stop incrementing through the tail, giving the appearance of paused.
            {

                Line line = new Line();
                line.start.X = trail[trailStartMarker].start.X;// * read current line from array
                line.start.Y = trail[trailStartMarker].start.Y;
                line.end.X = trail[trailStartMarker].end.X;
                line.end.Y = trail[trailStartMarker].end.Y;
                line.colorIndex = trail[trailStartMarker].colorIndex;
                // * 
                line = SetLineToNewCoords(line, width, height);//moves line.  Also checks for out-of-bounds and reverses direction
                                                               // * --check if random color is turned on.  if it is, set new colors 1% chance on each line drawn
                                                               // * --if trail length / 3 lines have been drawn, loop through the three available colors.
                IncrementMarkers();// * increment indexes
                trail[trailStartMarker] = line;// * store new workline into array at new index
                trail[trailStartMarker].start.X = line.start.X;
                trail[trailStartMarker].start.Y = line.start.Y;
                trail[trailStartMarker].end.X = line.end.X;
                trail[trailStartMarker].end.Y = line.end.Y;
                trail[trailStartMarker].colorIndex = line.colorIndex;
            }
            // * draw the trail
            //for (int dl = trailLength; dl >= 0; dl--)
            for (int dl = 0; dl <= trailLength; dl++)
            {

                int index = trailStartMarker - (trailLength - dl);
                if (index >= maxTrailLength) { index -= maxTrailLength; }
                if (index < 0) { index += maxTrailLength; }

                Pen localPen;
                if (classicErase && dl == 0)
                {
                    localPen = new Pen(Color.Black, penThickness[trail[index].colorIndex]);
                }
                else
                {
                    if (gradientMode)
                        localPen = new Pen(trail[index].rgbColor, penThickness[trail[index].colorIndex]);
                    else
                        localPen = new Pen(Drawing.lstColors[Drawing.colorIndexes[trail[index].colorIndex]], penThickness[trail[index].colorIndex]);
                }

                if (classicErase == false || dl == trailLength || dl == 0) //basically don't draw the intermediate lines if in classic mode
                {
                    int hDivide = hMirror ? divisor : 1; //tells it to make the width half-size unless overridden by collision divisor
                    int vDivide = vMirror ? divisor : 1; //same as above but vertical
                    int hReverseOffset = reverse ? width : 0; //either will subtract the negative width or zero to reverse lines
                    int vReverseOffset = reverse ? height : 0; //either will subtract the negative width or zero to reverse lines
                    Vector2 startBase = new Vector2(trail[index].start.X , trail[index].start.Y );
                    Vector2 endBase = new Vector2(trail[index].end.X , trail[index].end.Y );
                    Vector2 start = new Vector2(startBase.X, startBase.Y);
                    Vector2 end = new Vector2(endBase.X, endBase.Y);

                    if (hDivide > 1)
                    {
                        start = Calculations.ShrinkHoriz(start);
                        end = Calculations.ShrinkHoriz(end);
                    }
                    if (vDivide > 1) 
                    {
                        start = Calculations.ShrinkVert(start); 
                        end = Calculations.ShrinkVert(end); 
                    }
                    if (drawMode == 1)
                        DrawRectangle(spriteBatch, start, end, localPen);
                    else if (drawMode == 0)
                        //upper left line
                        DrawLine(spriteBatch, start, end, localPen);
        //            else if (drawMode == 2)
         //               g.DrawEllipse(localPen, r);
                    if (hMirror && vMirror)
                    {
                        start = startBase;
                        end = endBase;
                        if (vDivide > 1)
                        {
                            start = Calculations.ShrinkVert(start);
                            end = Calculations.ShrinkVert(end);
                        }
                        if (hDivide > 1)
                        {
                            start = Calculations.ShrinkHoriz(start);
                            end = Calculations.ShrinkHoriz(end);
                        }
                        start = Calculations.FlipHoriz(Calculations.FlipVert(start));
                        end = Calculations.FlipHoriz(Calculations.FlipVert(end));
                        //lower right line
                        if (drawMode == 1)
                            DrawRectangle(spriteBatch, start, end, localPen);
                        else if (drawMode == 0)
                            DrawLine(spriteBatch, start, end, localPen);
                        // else if (drawMode == 2)
                        //   g.DrawEllipse(localPen, width - r.X - r.Width, height - r.Y - r.Height, r.Width, r.Height);
                        //if (reverse)
                        //{
                        //    //if double-mirrored, rotate ends for reversal instead of standard mirror
                        //    hReverseOffset = 0;
                        //    vReverseOffset = 0;
                        //    //rotate 90 degrees by swapping x and y.  This requires scaling by aspect ratio.
                        //    start.X = trail[index].start.Y * width / height / divisor;
                        //    start.Y = trail[index].start.X * height / width / divisor;
                        //    end.X = trail[index].end.Y * width / height / divisor;
                        //    end.Y = trail[index].end.X * height / width / divisor;
                        //    //now do same code for rectangles
                        //    r.X = (int)start.X;
                        //    r.Y = (int)start.Y;
                        //    r.Width = (int)Math.Abs(end.X - start.X);
                        //    r.Height = (int)Math.Abs(end.Y - start.Y);
                        //    if (start.X > end.X) r.X -= r.Width;
                        //    if (start.Y > end.Y) r.Y -= r.Height;
                        //}
                    }
                    if (hMirror == true)
                    {
                        //                        //upper-right line (or right side)
                        if (drawMode == 1)
                        //    //because rectangles have a width, i have to remove width from xpos to "reverse" the rectangle
                          DrawRectangle(spriteBatch, new Vector2(width - start.X, Math.Abs(vReverseOffset - start.Y)), new Vector2(width - end.X, Math.Abs(vReverseOffset - end.Y)), localPen);
                        else if (drawMode == 0)
                          DrawLine(spriteBatch, new Vector2(width - start.X, Math.Abs(vReverseOffset - start.Y)), new Vector2(width - end.X, Math.Abs(vReverseOffset - end.Y)), localPen);

                        //DrawLine(spriteBatch, width - start.X, Math.Abs(vReverseOffset - start.Y), width - end.X, Math.Abs(vReverseOffset - end.Y));
                        //else if (drawMode == 2)
                        //    g.DrawEllipse(localPen, width - r.X - r.Width, Math.Abs(vReverseOffset - r.Y), r.Width, r.Height);
                    }
                    if (vMirror == true)
                    {
//                        //lower-right line (or bottom)
                       if (drawMode == 1)
                            //           
                            DrawRectangle(spriteBatch, new Vector2(Math.Abs(start.X), height - start.Y), new Vector2(end.X, height - end.Y), localPen);
                        else if (drawMode == 0)
                            DrawLine(spriteBatch, new Vector2(Math.Abs(start.X), height - start.Y), new Vector2( end.X, height - end.Y), localPen);
                        //                        else
                        //                            g.DrawEllipse(localPen, Math.Abs(hReverseOffset - r.X), height - r.Y - r.Height, r.Width, r.Height);
                    }

                }
                
            }
        }

        //this function is for incrementing the counter loops that track the draw and erase lines in the trail array.
        public static void IncrementMarkers()
        {
            if (trailStartMarker < maxTrailLength - 1)
                trailStartMarker++;
            else
                trailStartMarker = 0;
            if (trailEndMarker - trailLength >= 0)//i did this in weird order to prevent the value from EVER going below zero.  The drawing is asynchronous so this matters.
            {
                trailEndMarker = trailStartMarker - trailLength;
            }
            else
            {
                trailEndMarker = maxTrailLength - (trailEndMarker - trailLength);
            }
        }
        //used for the rewind function.
        public static void DecrementMarkers()
        {
            if (trailStartMarker > 0)
                trailStartMarker--;
            else
                trailStartMarker = maxTrailLength - 1;
            if (trailEndMarker - trailLength >= 0)//i did this in weird order to prevent the value from EVER going below zero.  The drawing is asynchronous so this matters.
            {
                trailEndMarker = trailStartMarker - trailLength;
            }
            else
            {
                trailEndMarker = maxTrailLength - (trailEndMarker - trailLength);
            }
        }
        /*
         * This function redraws the last 150 lines, including erasings.  It's purpose is to refresh the view after a clear. 
         * It can be called in any function as it doesn't actually run unless the correct mode is set.
         * todo:  Put the screen erase in here.
         */
        public static void Redraw(SpriteBatch spriteBatch)
        {

            if (classicErase == true)
            {
                bool storedPaused = paused;
                paused = true; //so it keeps the same lines when incrementing
                for (int i = 0; i < 150; i++)
                    DecrementMarkers(); //rewind 150 lines
                for (int i = 0; i < 150; i++)
                {
                    IncrementMarkers();
                    DrawTrail(spriteBatch); //redraw them

                }
                paused = storedPaused;
            }

        }

        /**
         * this function has multiple purposes
         * It takes a passed in line, looks at the magnitude variables (directions)
         * and uses those values to change the coordinates of the line before returning it.
         * the line struct also contains a color index so this function increments that if needed.
         * Finally, after the incrementing is done of all coordinates, 
         * the function verifies no new numbers are out of bounds.  If they are, then
         * they are reset to max or min appropriately and the magnitude is reversed in direction
         * for that axis, then set to a new magnitude (speed).
         */
        public static Line SetLineToNewCoords(Line line, int maxWidth, int maxHeight)
        {
            //this also handles random magnitude changes on wall hits
            line.start.X += Magnitude.start.X;
            if (line.start.X < 0)
            {
                line.start.X = 0; //you can draw out of bounds but this prevents errors
                Magnitude.start.X = rnd.Next(minIncrement, maxIncrement);
            }
            else if (line.start.X > maxWidth)
            {
                line.start.X = maxWidth;
                Magnitude.start.X = (rnd.Next(minIncrement, maxIncrement)) * -1;
            }

            line.start.Y += Magnitude.start.Y;
            if (line.start.Y < 0)
            {
                line.start.Y = 0; //you can draw out of bounds but this prevents errors
                Magnitude.start.Y = rnd.Next(minIncrement, maxIncrement);
            }
            else if (line.start.Y > maxHeight)
            {
                line.start.Y = maxHeight;
                Magnitude.start.Y = (rnd.Next(minIncrement, maxIncrement)) * -1;
            }

            line.end.X += Magnitude.end.X;
            if (line.end.X < 0)
            {
                line.end.X = 0; //you can draw out of bounds but this prevents errors
                Magnitude.end.X = rnd.Next(minIncrement, maxIncrement);
            }
            else if (line.end.X > maxWidth)
            {
                line.end.X = maxWidth;
                Magnitude.end.X = (rnd.Next(minIncrement, maxIncrement)) * -1;
            }

            line.end.Y += Magnitude.end.Y;
            if (line.end.Y < 0)
            {
                line.end.Y = 0; //you can draw out of bounds but this prevents errors
                Magnitude.end.Y = rnd.Next(minIncrement, maxIncrement);
            }
            else if (line.end.Y > maxHeight)
            {
                line.end.Y = maxHeight;
                Magnitude.end.Y = (rnd.Next(minIncrement, maxIncrement)) * -1;
            }

            // * --note that trail drawing contains the code to adjust for mirrored and split
            //check steps towards next color.  Switch if reached and generate new #
            colorStepsTaken++;
            if (colorStepsTaken >= nextColorCycle)
            {
                colorStepsTaken = 0;
                nextColorCycle = rnd.Next(20, 100);
                line.colorIndex++;
                if (gradientMode) SetPen(rnd.Next(0, 15), (line.colorIndex + 2) % 3);  //because two of the colors are needed for gradient, i randomize the 3rd
                if (line.colorIndex > 2)
                {
                    line.colorIndex = 0;

                }

            }

            /*
             * The following block of code is quite possibly the most mathematically complex of the entire app.
             * It takes the current color and the future color, then breaks them each down into their component RGB 
             * values.  It then looks at how many lines will be drawn from the current color starting and when the
             * next color appears.  It divides the difference in each color value by the number of steps.
             * Finally, it multiplies that value by the number of steps already taken and adds the start color to it.
             * After combining this color into rgb, you get the new gradient color.
             */
            Color startColor = (Color)lstColors[Drawing.colorIndexes[line.colorIndex]];//starting color of gradient
            int nextIndex = line.colorIndex + 1;
            if (nextIndex > 2) nextIndex = 0;
            Color endColor = (Color)lstColors[Drawing.colorIndexes[nextIndex]];//starting color of gradient
            int CurrentR = (int)(((double)(endColor.R - startColor.R) / (double)nextColorCycle) * colorStepsTaken) + startColor.R;
            int CurrentG = (int)(((double)(endColor.G - startColor.G) / (double)nextColorCycle) * colorStepsTaken) + startColor.G;
            int CurrentB = (int)(((double)(endColor.B - startColor.B) / (double)nextColorCycle) * colorStepsTaken) + startColor.B;
            line.rgbColor = new Color(CurrentR, CurrentG, CurrentB);
            return line;

        }



    }
}
